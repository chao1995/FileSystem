package FileSystem;

import java.nio.ByteBuffer;

class BitMapBlock extends Block {

	public static int[] mask;
	public static int[] freeMask;
	private byte[] bitMap; 

	
	// constructor
	public BitMapBlock() {
		// populate mask
		mask = new int[Spec.MASK_SIZE];
		mask[Spec.MASK_SIZE - 1] = 1;
		for (int i = Spec.MASK_SIZE - 2; i >= 0; i--) {
			mask[i] = mask[i+1] * 2;
		}
		// populate freeMask
		freeMask = new int[Spec.MASK_SIZE];
		for (int i = 0; i < Spec.MASK_SIZE; i++) {
			freeMask[i] = ~mask[i];
		}
		// initialize bit map
		// note the actual data of bit map may not be the same as the byte[]'s length
		bitMap = new byte[Spec.BLOCK_SIZE_IN_BYTE];
		// reserve bitmap block and descriptor blocks
		for (int i = 0; i < Spec.BITMAP_BLOCK_AMOUNT + Spec.DESCRIPTOR_BLOCK_AMOUNT; i++) {
			useBlock(i);
		}
	}

	public BitMapBlock(byte[] bitMapBlockData) {
		this();
		// initialize bit map
		assert(bitMapBlockData.length == Spec.BLOCK_SIZE_IN_BYTE);
		this.bitMap = bitMapBlockData;
	}

	
	// methods
	@Override
	public byte[] getBytes() {
		return bitMap;
	}

	/*
	 * @return the fist free data block's id, or -1 if no free data blocks available 
	 */
	public int getFreeBlock() {
		ByteBuffer wrapped = ByteBuffer.wrap(bitMap);
		for (int i = 0; i < Spec.BIT_MAP_LENGTH; i++) {
			int anInt = wrapped.getInt();
			for (int j = 0; j < Spec.MASK_SIZE; j++) {
				// mask[j] only has 1 in the jth bit
				// so, if andValue = 0, anInt got an 0 in the jth bit 
				int andValue = anInt & mask[j];
				if (andValue == 0) {
					return i*Spec.MASK_SIZE + j;
				}
			}
		}
		return -1;
	}
	
	public void useBlock(int blockId) {
		assert (0 <= blockId && blockId < Spec.TOTAL_BLOCK_AMOUNT);
		// init
		int mapIndex = (blockId < Integer.SIZE) ? 0 : 1;
		ByteBuffer wrapped = ByteBuffer.wrap(bitMap);
		ByteBuffer buffer = ByteBuffer.allocate(bitMap.length);
		// seek to mapIndex and replace the int at that indexed point of bit map
		for (int i = 0; i < Spec.BIT_MAP_INTS_AMOUNT; i++) {
			int anInt = wrapped.getInt();
			if (i == mapIndex) {
				buffer.putInt(anInt | mask[blockId % Integer.SIZE]);
			} else {
				buffer.putInt(anInt);
			}
		}
		// reset bit map
		bitMap = buffer.array();
	}
	
	public void freeBlock(int blockId) {
		assert (0 <= blockId && blockId < Spec.BIT_MAP_LENGTH * Spec.MASK_SIZE);
		// init
		int mapIndex = (blockId < Integer.SIZE) ? 0 : 1;
		ByteBuffer wrapped = ByteBuffer.wrap(bitMap);
		ByteBuffer buffer = ByteBuffer.allocate(bitMap.length);
		// seek to mapIndex and replace the int at that indexed point of bit map
		for (int i = 0; i < Spec.BIT_MAP_INTS_AMOUNT; i++) {
			int anInt = wrapped.getInt();
			if (i == mapIndex) {
				buffer.putInt(anInt & freeMask[blockId % Integer.SIZE]);
			} else {
				buffer.putInt(anInt);
			}
		}
		// reset bit map
		bitMap = buffer.array();
	}

}