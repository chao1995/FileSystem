package FileSystem;

abstract class Block {

	public abstract byte[] getBytes();
	
}
