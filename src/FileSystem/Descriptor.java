package FileSystem;

import java.nio.ByteBuffer;

class Descriptor {
	private int fileLength;
	private int[] blockIds;
	
	// constructor
	public Descriptor() {
		this(new byte[Spec.DESCRIPTOR_SIZE_IN_BYTE]);
	}
	
	public Descriptor(byte[] descriptorData) {
		assert (descriptorData.length == Spec.DESCRIPTOR_SIZE_IN_BYTE);
		this.blockIds = new int[Spec.DESCRIPTOR_BLOCK_ID_AMOUNT];
		ByteBuffer wrapped = ByteBuffer.wrap(descriptorData);
		setFileLength(wrapped.getInt());
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_ID_AMOUNT; i++) {
			blockIds[i] = wrapped.getInt();
		}
	}

	// methods
	public void addBlockId(int blockId) {
		// assert blockId refers to a dataBlock's id, which is [7, 63]
		assert(Spec.INDEX_DESCRIPTOR_BLOCK_END <= blockId && blockId < Spec.TOTAL_BLOCK_AMOUNT);
		for (int i = 0; i < blockIds.length; i++) {
			int bid = blockIds[i];
			if (bid == 0) {
				blockIds[i] = blockId;
				return;
			}
		}
		System.out.println("This file has used 3 blocks.");
	}

	public int getBlockId(int index) {
		assert (0 <= index && index < blockIds.length);
		return blockIds[index];
	}
	
	public byte[] getBytes() {
		ByteBuffer wrapped = ByteBuffer.allocate(Spec.DESCRIPTOR_SIZE_IN_BYTE);
		wrapped.putInt(getFileLength());
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_ID_AMOUNT; i++) {
			wrapped.putInt(blockIds[i]);
		}
		return wrapped.array();
	}

	public int getFileLength() {
		return fileLength;
	}

	public void setFileLength(int fileLength) {
		this.fileLength = fileLength;
	}

	public boolean isReal() {
		return (blockIds[0] == 0) ? false : true;
	}
}