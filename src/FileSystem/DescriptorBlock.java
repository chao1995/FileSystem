package FileSystem;

import java.util.Arrays;


class DescriptorBlock extends Block {

	private Descriptor[] descriptors;
	
	// constructors
	public DescriptorBlock() {
		this(new byte[Spec.BLOCK_SIZE_IN_BYTE]);
	}
	
	public DescriptorBlock(byte[] blockData) {
		assert(blockData != null && blockData.length == Spec.BLOCK_SIZE_IN_BYTE);
		// populate descriptors
		this.descriptors = new Descriptor[Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT];
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT; i++) {
			byte[] descriptorData = Arrays.copyOfRange(blockData, i*Spec.DESCRIPTOR_SIZE_IN_BYTE, (i+1)*Spec.DESCRIPTOR_SIZE_IN_BYTE);
			Descriptor descriptor = new Descriptor(descriptorData);
			descriptors[i] = descriptor;
		}
	}
	
	// methods
	public Descriptor getDescriptor(int relativeDescriptorId) {
		assert(0 <= relativeDescriptorId && relativeDescriptorId < Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT);
		return descriptors[relativeDescriptorId];
	}

	public void removeDescriptor(int relativeDescriptorId) {
		assert(0 <= relativeDescriptorId && relativeDescriptorId < Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT);
		descriptors[relativeDescriptorId] = new Descriptor();
	}
	
	@Override
	public byte[] getBytes() {
		byte[] descriptorBlockDatas = new byte[Spec.BLOCK_SIZE_IN_BYTE]; 
		for (int i = 0; i < descriptors.length; i++) {
			Descriptor descriptor = descriptors[i];
			byte[] descriptorDatas = descriptor.getBytes();
			assert(descriptorDatas.length == Spec.DESCRIPTOR_SIZE_IN_BYTE);
			for (int j = 0; j < descriptorDatas.length; j++) {
				descriptorBlockDatas[i * Spec.DESCRIPTOR_SIZE_IN_BYTE + j] = descriptorDatas[j];
			}
		}
		return descriptorBlockDatas;
	}

	public int getFreeDescriptorIndex() {
		for (int i = 0; i < descriptors.length; i++) {
			Descriptor descriptor = this.descriptors[i];
			if (!descriptor.isReal()) {
				return i;
			}
		}
		return -1;
	}
	
}