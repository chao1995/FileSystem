package FileSystem;

import java.util.Arrays;

public class DirectoryDataBlock extends Block {

	private DirectoryEntry[] entries;
	
	// constructor
	public DirectoryDataBlock(byte[] directoryDataBlockDatas) {
		assert(directoryDataBlockDatas != null && directoryDataBlockDatas.length == Spec.BLOCK_SIZE_IN_BYTE);
		// populate entries
		this.entries = new DirectoryEntry[Spec.DIRECTORY_DATA_BLOCK_ENTRY_AMOUNT];
		for (int i = 0; i < Spec.DIRECTORY_DATA_BLOCK_ENTRY_AMOUNT; i++) {
			byte[] entryData = Arrays.copyOfRange(directoryDataBlockDatas, i*Spec.ENTRY_SIZE_IN_BYTE, (i+1)*Spec.ENTRY_SIZE_IN_BYTE);
			DirectoryEntry entry = new DirectoryEntry(entryData);
			entries[i] = entry;
		}
	}

	// methods
	@Override
	public byte[] getBytes() {
		byte[] directoryDataBlockDatas = new byte[Spec.BLOCK_SIZE_IN_BYTE]; 
		for (int i = 0; i < entries.length; i++) {
			DirectoryEntry entry = entries[i];
			byte[] entryDatas = entry.getBytes();
			assert(entryDatas.length == Spec.ENTRY_SIZE_IN_BYTE);
			for (int j = 0; j < entryDatas.length; j++) {
				directoryDataBlockDatas[i * Spec.ENTRY_SIZE_IN_BYTE + j] = entryDatas[j];
			}
		}
		return directoryDataBlockDatas;
	}

	public int findEntry(String fileName) {
		for (int i = 0; i < entries.length; i++) {
			DirectoryEntry entry = entries[i];
			if (entry.getFileName().equals(fileName)) {
				return i;
			}
		}
		return -1;
	}
	
	public DirectoryEntry getEntry(int index) {
		assert (0 <= index && index < entries.length);
		return entries[index];
	}
	
	public void removeEntry(int index) {
		assert (0 <= index && index < entries.length);
		entries[index] = new DirectoryEntry();
	}
	
	public int getFreeEntryIndex() {
		for (int i = 0; i < this.entries.length; i++) {
			DirectoryEntry entry = this.entries[i];
			if (!entry.isReal()) {
				return i;
			}
		}
		return -1;
	}
	
	public void insertEntry(int index, String fileName, int descriptorId) {
		assert(entries[index].isReal() == false);
		entries[index] = new DirectoryEntry(fileName, descriptorId);
	}

	public boolean isFileExist(String fileName) {
		for (int i = 0; i < this.entries.length; i++) {
			DirectoryEntry entry = this.entries[i];
			if (entry.isReal() && entry.getFileName().equals(fileName)) {
				return true;
			}
		}
		return false;
	}
	
}
