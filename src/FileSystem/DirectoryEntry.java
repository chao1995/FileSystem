package FileSystem;

import java.nio.ByteBuffer;

class DirectoryEntry {
	private String fileName;
	private int descriptorId;

	// constructors
	public DirectoryEntry() {
		this(new byte[Spec.ENTRY_SIZE_IN_BYTE]);
	}

	public DirectoryEntry(byte[] entryDatas) {
		assert (entryDatas.length == Spec.ENTRY_SIZE_IN_BYTE);
		ByteBuffer wrapped = ByteBuffer.wrap(entryDatas);
		byte[] fileNameBytes = new byte[Spec.FILE_NAME_MAX_LENGTH]; 
		wrapped.get(fileNameBytes, 0, Spec.FILE_NAME_MAX_LENGTH);
		String fileName = new String(fileNameBytes);
		this.fileName = fileName.trim();
		this.setDescriptorId(wrapped.getInt());
	}
	
	public DirectoryEntry(String fileName, int descriptorId) {
		assert(Spec.INDEX_DESCRIPTOR_BLOCK_END <= descriptorId && descriptorId < Spec.TOTAL_BLOCK_AMOUNT);
		if (fileName.length() > Spec.FILE_NAME_MAX_LENGTH) {
			fileName = fileName.substring(0, Spec.FILE_NAME_MAX_LENGTH);
		}
		this.fileName = fileName;
		this.setDescriptorId(descriptorId);
	}
	
	// methods
	public byte[] getBytes() {
		if (!isReal()) {
			return new byte[Spec.ENTRY_SIZE_IN_BYTE];
		}
		ByteBuffer wrapped = ByteBuffer.allocate(Spec.ENTRY_SIZE_IN_BYTE);
		String fileName = this.fileName;
		while (fileName.length() < Spec.FILE_NAME_MAX_LENGTH) {
			fileName += " ";
		}
		assert(fileName.getBytes().length == Spec.FILE_NAME_MAX_LENGTH);
		wrapped.put(fileName.getBytes());
		wrapped.putInt(this.getDescriptorId());
		return wrapped.array();
	}
	
	// getter and setter
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public boolean isReal() {
		return getDescriptorId() != 0;
	}
	
	public int getDescriptorId() {
		return descriptorId;
	}
	
	public void setDescriptorId(int descriptorId) {
		this.descriptorId = descriptorId;
	}
	
}
