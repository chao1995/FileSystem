package FileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import IOSystem.LDisk;

public class FileSystem {

	private LDisk disk;
	// instance variables that are added for efficiency
	// they are created and destroyed in in & sv methods respectively
	private BitMapBlock bitMapBlock;
	private OpenFileTable oft;
	private DescriptorBlock[] descriptorBlocks;
	private DirectoryDataBlock[] directoryDataBlocks;

	// File System interface
	public void create(String fileName) {
		// find a FREE descriptor for the file
		int descriptorBlockIndex = 0;
		int relativeDescriptorId = 0;
		int descriptorId = -1;
		for (int i = 0; i < descriptorBlocks.length; i++, descriptorBlockIndex++) {
			DescriptorBlock descriptorBlock = descriptorBlocks[i];
			int index = descriptorBlock.getFreeDescriptorIndex();
			if (index != -1) {
				descriptorId = i * Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT + index;
				relativeDescriptorId = index;
				break;
			}
		}
		// can find a free descriptor
		if (descriptorId != -1) {
			// find a FREE entry in directoryDataBlock
			for (int i = 0; i < directoryDataBlocks.length; i++) {
				DirectoryDataBlock directoryDataBlock = directoryDataBlocks[i];
				int index = directoryDataBlock.getFreeEntryIndex();
				if (index != -1) {
					// check fileName duplicate
					boolean isDuplicate = directoryDataBlock.isFileExist(fileName);
					if (isDuplicate) {
						System.out.println("error");
						break;
					}
					// fill entry
					directoryDataBlock.insertEntry(index, fileName, descriptorId);
					// fill descriptor
					DescriptorBlock descriptorBlock = this.descriptorBlocks[descriptorBlockIndex];
					Descriptor descriptor = descriptorBlock.getDescriptor(relativeDescriptorId);
					descriptor.setFileLength(0);
					for (int j = 0; j < Spec.DESCRIPTOR_BLOCK_ID_AMOUNT; j++) {
						int freeBlockId = this.bitMapBlock.getFreeBlock();
						this.bitMapBlock.useBlock(freeBlockId);
						descriptor.addBlockId(freeBlockId);
					}
					// prompt message
					System.out.println(fileName + " created");
					break;
				}
			}
		} else {
			System.out.println("error: directory is full");
		}
	}

	public void destroy(String fileName) {
		for (int i = 0; i < directoryDataBlocks.length; i++) {
			DirectoryDataBlock directoryDataBlock = directoryDataBlocks[i];
			int index = directoryDataBlock.findEntry(fileName);
			if (index != -1) {
				DirectoryEntry entry = directoryDataBlock.getEntry(index);
				int descriptorId = entry.getDescriptorId();
				// check whether file is in use
				int tableEntryIndex = this.oft.getEntryIndexWithDescriptorId(descriptorId);
				if (tableEntryIndex != -1) {
					System.out.println("error: file in use");
					break;
				}
				// file is not in use
				int descriptorBlockIndex = 0;
				while (descriptorId >= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT) {
					descriptorBlockIndex++;
					descriptorId -= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT;
				}
				DescriptorBlock descriptorBlock = this.descriptorBlocks[descriptorBlockIndex];
				descriptorBlock.removeDescriptor(descriptorId);
				directoryDataBlock.removeEntry(index);
				// prompt message
				System.out.println(fileName + " destroyed");
				break;
			}
		}
	}

	public int open(String fileName) {
		int oftIndex = -1;
		for (int i = 0; i < directoryDataBlocks.length; i++) {
			DirectoryDataBlock directoryDataBlock = directoryDataBlocks[i];
			int index = directoryDataBlock.findEntry(fileName);
			if (index != -1) {
				DirectoryEntry entry = directoryDataBlock.getEntry(index);
				int descriptorId = entry.getDescriptorId();
				int relativeDescriptorId = descriptorId;
				int descriptorBlockIndex = 0;
				while (relativeDescriptorId >= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT) {
					descriptorBlockIndex++;
					relativeDescriptorId -= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT;
				}
				DescriptorBlock descriptorBlock = this.descriptorBlocks[descriptorBlockIndex];
				Descriptor descriptor = descriptorBlock.getDescriptor(relativeDescriptorId);
				int blockId = descriptor.getBlockId(0);
				// get a free table entry in OpenFileTable
				byte[] readBuffer = new byte[Spec.BLOCK_SIZE_IN_BYTE]; 
				this.disk.readBlock(blockId, readBuffer);
				int currentPosition = 0;
				int fileLength = descriptor.getFileLength();
				oftIndex = oft.insertEntry(readBuffer, currentPosition, descriptorId, fileLength);
				if (oftIndex != -1) {
					break;
				}
			}
		}
		if (oftIndex != -1) {
			System.out.println(fileName + " opened " + oftIndex);
		} else {
			System.out.println("error: open file table is full");
		}
		return oftIndex;
	}

	public void close(int oftIndex) {
		close(oftIndex, false);
	}

	private void close(int oftIndex, boolean silent) {
		// check whether oftIndex is valid
		if (oftIndex < 0 || oftIndex > Spec.OPEN_FILE_TABLE_ENTRY_AMOUNT) {
			System.out.println("error");
			return;
		}
		TableEntry entry = this.oft.getEntry(oftIndex);
		int currentPosition = entry.getCurrentPosition();
		// get descriptor with descriptorId
		int descriptorId = entry.getDescriptorId();
		int relativeDescriptorId = descriptorId;
		int descriptorBlockIndex = 0;
		while (relativeDescriptorId >= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT) {
			descriptorBlockIndex++;
			relativeDescriptorId -= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT;
		}
		DescriptorBlock descriptorBlock = this.descriptorBlocks[descriptorBlockIndex];
		Descriptor descriptor = descriptorBlock.getDescriptor(relativeDescriptorId);		
		// get current block id in use
		int blockIdIndex = 0;
		blockIdIndex = currentPosition / Spec.BLOCK_SIZE_IN_BYTE;
		int blockIdInUse = descriptor.getBlockId(blockIdIndex);
		this.disk.writeBlock(blockIdInUse, entry.getRwBuffer());
		this.oft.removeEntry(oftIndex);
		if (!silent) {
			System.out.println(oftIndex + " closed");
		}
	}

	public void read(int oftIndex, int count) {
		// check whether oftIndex is valid
		if (oftIndex < 0 || oftIndex > Spec.OPEN_FILE_TABLE_ENTRY_AMOUNT) {
			System.out.println("error");
			return;
		}
		// check whether some file is opened at oftIndex
		TableEntry entry = this.oft.getEntry(oftIndex);
		if (entry == null) {
			System.out.println("error");
			return;
		}
		// everything is OK, read
		int fileLength = entry.getFileLength();
		int currentPosition = entry.getCurrentPosition();
		// check if exceed read
		if (currentPosition + count >= fileLength) {
			count = fileLength - currentPosition;
		}
		// file is long enough to be read from currentPosition
		String string = "";
		while (count > 0) {
			// check whether reach the end of the buffer
			if (currentPosition != 0 && currentPosition % Spec.BLOCK_SIZE_IN_BYTE == 0) {
				// get descriptor with descriptorId
				int descriptorId = entry.getDescriptorId();
				int relativeDescriptorId = descriptorId;
				int descriptorBlockIndex = 0;
				while (relativeDescriptorId >= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT) {
					descriptorBlockIndex++;
					relativeDescriptorId -= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT;
				}
				DescriptorBlock descriptorBlock = this.descriptorBlocks[descriptorBlockIndex];
				Descriptor descriptor = descriptorBlock.getDescriptor(relativeDescriptorId);
				// get current block id in use
				int blockIdIndex = 0;
				blockIdIndex = currentPosition / Spec.BLOCK_SIZE_IN_BYTE;
				if (currentPosition != 0 && currentPosition % Spec.BLOCK_SIZE_IN_BYTE == 0) {
					blockIdIndex -= 1;
				}
				int blockIdInUse = descriptor.getBlockId(blockIdIndex);
				// write current buffer to disk
				byte[] buffer = entry.getRwBuffer();
				this.disk.writeBlock(blockIdInUse, buffer);
				// replace current buffer
				int nextBlockId = descriptor.getBlockId(blockIdIndex + 1);
				this.disk.readBlock(nextBlockId, buffer);
			}
			// read from currentPosition with available amount of bytes
			byte[] rwBuffer = entry.getRwBuffer();
			int available = Spec.BLOCK_SIZE_IN_BYTE - currentPosition % Spec.BLOCK_SIZE_IN_BYTE;
			int length = (count < available) ? count : available;
			string += new String(Arrays.copyOfRange(rwBuffer, currentPosition % Spec.BLOCK_SIZE_IN_BYTE, currentPosition % Spec.BLOCK_SIZE_IN_BYTE + length));
			// update table entry
			entry.setCurrentPosition(currentPosition += length);
			// decrement count
			count -= length;
		}
		System.out.println(string);
	}

	public void write(int oftIndex, String character, int count) {
		assert (character.length() == 1);
		// check whether oftIndex is valid
		if (oftIndex < 0 || oftIndex > Spec.OPEN_FILE_TABLE_ENTRY_AMOUNT) {
			System.out.println("error");
			return;
		}
		// check whether some file is opened at oftIndex
		TableEntry entry = this.oft.getEntry(oftIndex);
		if (entry == null) {
			System.out.println("error");
			return;
		}
		// everything is OK, write
		int currentPosition = entry.getCurrentPosition();
		// check if exceed read
		if (currentPosition + count > Spec.MAX_FILE_LENGTH) {
			System.out.println("error");
			return;
		}
		// file is long enough to be written from currentPosition
		int totalBytesToWrite = count;
		while (count > 0) {
			// get descriptor with descriptorId
			int descriptorId = entry.getDescriptorId();
			int relativeDescriptorId = descriptorId;
			int descriptorBlockIndex = 0;
			while (relativeDescriptorId >= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT) {
				descriptorBlockIndex++;
				relativeDescriptorId -= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT;
			}
			DescriptorBlock descriptorBlock = this.descriptorBlocks[descriptorBlockIndex];
			Descriptor descriptor = descriptorBlock.getDescriptor(relativeDescriptorId);
			// check whether reach the end of the buffer
			if (currentPosition != 0 && currentPosition % Spec.BLOCK_SIZE_IN_BYTE == 0) {
				// get current block id in use
				int blockIdIndex = 0;
				blockIdIndex = currentPosition / Spec.BLOCK_SIZE_IN_BYTE;
				if (currentPosition != 0 && currentPosition % Spec.BLOCK_SIZE_IN_BYTE == 0) {
					blockIdIndex -= 1;
				}
				int blockIdInUse = descriptor.getBlockId(blockIdIndex);
				// write current buffer to disk
				byte[] buffer = entry.getRwBuffer();
				this.disk.writeBlock(blockIdInUse, buffer);
				// replace current buffer
				int nextBlockId = descriptor.getBlockId(blockIdIndex + 1);
				this.disk.readBlock(nextBlockId, buffer);
			}
			// keep data before currentPosition
			byte[] rwBuffer = entry.getRwBuffer();
			ByteBuffer wrapped = ByteBuffer.allocate(Spec.BLOCK_SIZE_IN_BYTE);
			wrapped.put(Arrays.copyOfRange(rwBuffer, 0, currentPosition % Spec.BLOCK_SIZE_IN_BYTE));
			// write from currentPosition with available amount of bytes
			int available = Spec.BLOCK_SIZE_IN_BYTE - currentPosition % Spec.BLOCK_SIZE_IN_BYTE;
			int length = (count < available) ? count : available;
			String string = "";
			for (int i = 0; i < length; i++) {
				string += character;
			}
			wrapped.put(string.getBytes());
			// update currentPosition
			currentPosition += length;
			entry.setCurrentPosition(currentPosition);
			// keep data after write
			if (currentPosition != 0 && currentPosition % Spec.BLOCK_SIZE_IN_BYTE != 0) {
				wrapped.put(Arrays.copyOfRange(rwBuffer, currentPosition % Spec.BLOCK_SIZE_IN_BYTE, Spec.BLOCK_SIZE_IN_BYTE));
			}
			// update rwBuffer
			entry.setRwBuffer(wrapped.array());
			// decrement count
			count -= length;
			// update file length
			descriptor.setFileLength(descriptor.getFileLength() + length);
			entry.setFileLength(entry.getFileLength() + length);
		}
		System.out.println(totalBytesToWrite + " bytes written");
	}

	public void lseek(int oftIndex, int pos) {
		assert (0 <= pos && pos < Spec.BLOCK_SIZE_IN_BYTE);
		// check whether oftIndex is valid
		if (oftIndex < 0 || oftIndex > Spec.OPEN_FILE_TABLE_ENTRY_AMOUNT) {
			System.out.println("error");
			return;
		}
		// check whether some file is opened at oftIndex
		TableEntry entry = this.oft.getEntry(oftIndex);
		if (entry == null) {
			System.out.println("error");
			return;
		}
		// everything is OK, going to seek
		int fileLength = entry.getFileLength();
		if (pos >= fileLength) {
			System.out.println("error");
			return;
		}
		// get descriptor with descriptorId
		int descriptorId = entry.getDescriptorId();
		int relativeDescriptorId = descriptorId;
		int descriptorBlockIndex = 0;
		while (relativeDescriptorId >= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT) {
			descriptorBlockIndex++;
			relativeDescriptorId -= Spec.DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT;
		}
		DescriptorBlock descriptorBlock = this.descriptorBlocks[descriptorBlockIndex];
		Descriptor descriptor = descriptorBlock.getDescriptor(relativeDescriptorId);
		// get currentPosition
		int currentPosition = entry.getCurrentPosition();
		// get current block id in use
		int blockIdIndex = 0;
		blockIdIndex = currentPosition / Spec.BLOCK_SIZE_IN_BYTE;
		int blockIdInUse = descriptor.getBlockId(blockIdIndex);
		// get block id wanted to seek to
		int blockIdToSeekIndex = 0;
		blockIdToSeekIndex = pos / Spec.BLOCK_SIZE_IN_BYTE;
		int blockIdToSeek = descriptor.getBlockId(blockIdToSeekIndex);
		// write current buffer to disk
		byte[] buffer = entry.getRwBuffer();
		this.disk.writeBlock(blockIdInUse, buffer);
		// seek
		this.disk.readBlock(blockIdToSeek, buffer);
		entry.setCurrentPosition(pos);
		System.out.println("position is " + pos);
	}

	public void directory() {
		for (int i = 0; i < directoryDataBlocks.length; i++) {
			DirectoryDataBlock directoryDataBlock = directoryDataBlocks[i];
			for (int j = 0; j < Spec.DIRECTORY_DATA_BLOCK_ENTRY_AMOUNT; j++) {
				DirectoryEntry entry = directoryDataBlock.getEntry(j);
				if (entry.isReal()) {
					System.out.println(entry.getFileName());
				}
			}
		}
	}

	public void init() {
		// initialize disk
		this.disk = new LDisk();
		// initialize BitMapBlock
		this.bitMapBlock = new BitMapBlock();
		// initialize descriptorBlocks
		this.descriptorBlocks = new DescriptorBlock[Spec.DESCRIPTOR_BLOCK_AMOUNT];
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_AMOUNT; i++) {
			this.descriptorBlocks[i] = new DescriptorBlock();
		}
		// initialize directory: take up 3 data blocks
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_ID_AMOUNT; i++) {
			int freeBlockId = this.bitMapBlock.getFreeBlock();
			if (freeBlockId != -1) {
				this.bitMapBlock.useBlock(freeBlockId);
				// update blockId in directory data blocks' *descriptor* (the first descriptor in block2)
				DescriptorBlock directoryDescriptorBlock = this.descriptorBlocks[0];
				Descriptor directoryFileDescriptor = directoryDescriptorBlock.getDescriptor(0);
				directoryFileDescriptor.addBlockId(freeBlockId);
				directoryFileDescriptor.setFileLength(directoryFileDescriptor.getFileLength() + Spec.BLOCK_SIZE_IN_BYTE);
			} else {
				System.out.println("Cannot init file system. Not enough free blocks.");
				System.exit(1);
			}
		}
		// initialize open file table && open "directory" file in open file table
		this.oft = new OpenFileTable();
		DescriptorBlock directoryDescriptorBlock = this.descriptorBlocks[0];
		Descriptor directoryFileDescriptor = directoryDescriptorBlock.getDescriptor(0);
		int firstDirectoryDataBlockId = directoryFileDescriptor.getBlockId(0);
		byte[] readBuffer = new byte[Spec.BLOCK_SIZE_IN_BYTE]; 
		this.disk.readBlock(firstDirectoryDataBlockId, readBuffer);
		int currentPosition = 0;
		int descriptorId = 0;
		int fileLength = directoryFileDescriptor.getFileLength();
		oft.insertEntry(readBuffer, currentPosition, descriptorId, fileLength);
		// initialize directoryDataBlocks
		this.directoryDataBlocks = new DirectoryDataBlock[Spec.DESCRIPTOR_BLOCK_ID_AMOUNT];
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_ID_AMOUNT; i++) {
			int directoryDataBlockId = directoryFileDescriptor.getBlockId(i);
			byte[] directoryDataBlockDatas = new byte[Spec.BLOCK_SIZE_IN_BYTE]; 
			this.disk.readBlock(directoryDataBlockId, directoryDataBlockDatas); 
			this.directoryDataBlocks[i] = new DirectoryDataBlock(directoryDataBlockDatas);
		}
		// finish. prompt message
		System.out.println("disk initialized");
	}

	public void init(String ldiskName) throws IOException {
		File file = new File(ldiskName);
		if (file.exists()) 
		{
			byte[][] blockDatas = new byte[Spec.TOTAL_BLOCK_AMOUNT][Spec.BLOCK_SIZE_IN_BYTE];

			// read blocks' data from file
			FileInputStream fis = new FileInputStream(new File(ldiskName));
			for (int i = 0; i < Spec.TOTAL_BLOCK_AMOUNT; i++) {
				byte[] buffer = new byte[Spec.BLOCK_SIZE_IN_BYTE];
				fis.read(buffer, 0, Spec.BLOCK_SIZE_IN_BYTE);
				blockDatas[i] = buffer;
			}
			fis.close();

			// initialize disk
			this.disk = new LDisk(blockDatas);
			// initialize BitMapBlock
			byte[] bitMapBlockDatas = blockDatas[Spec.INDEX_BITMAP_BLOCK]; 
			this.bitMapBlock = new BitMapBlock(bitMapBlockDatas);
			// initialize descriptorBlocks
			this.descriptorBlocks = new DescriptorBlock[Spec.DESCRIPTOR_BLOCK_AMOUNT];
			for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_AMOUNT; i++) {
				byte[] descriptorBlockDatas = blockDatas[Spec.INDEX_DESCRIPTOR_BLOCK_START + i];
				this.descriptorBlocks[i] = new DescriptorBlock(descriptorBlockDatas);
			}
			// SKIP: initialize directory for init with file

			// initialize open file table && open "directory" file in open file table
			this.oft = new OpenFileTable();
			DescriptorBlock directoryDescriptorBlock = this.descriptorBlocks[0];
			Descriptor directoryFileDescriptor = directoryDescriptorBlock.getDescriptor(0);
			int firstDirectoryDataBlockId = directoryFileDescriptor.getBlockId(0);
			byte[] readBuffer = new byte[Spec.BLOCK_SIZE_IN_BYTE]; 
			this.disk.readBlock(firstDirectoryDataBlockId, readBuffer);
			int currentPosition = 0;
			int descriptorId = 0;
			int fileLength = directoryFileDescriptor.getFileLength();
			this.oft.insertEntry(readBuffer, currentPosition, descriptorId, fileLength);
			// initialize directoryDataBlocks
			this.directoryDataBlocks = new DirectoryDataBlock[Spec.DESCRIPTOR_BLOCK_ID_AMOUNT];
			for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_ID_AMOUNT; i++) {
				int directoryDataBlockId = directoryFileDescriptor.getBlockId(i);
				byte[] directoryDataBlockDatas = new byte[Spec.BLOCK_SIZE_IN_BYTE]; 
				this.disk.readBlock(directoryDataBlockId, directoryDataBlockDatas); 
				this.directoryDataBlocks[i] = new DirectoryDataBlock(directoryDataBlockDatas);
			}
			// finish. prompt message
			System.out.println("disk restored");

		} else {
			init();
		}
	}

	public void saveTo(String ldiskName) throws IOException {
		// write BitMapBlock to disk
		byte[] bitMapBlockDatas = this.bitMapBlock.getBytes();
		this.disk.writeBlock(Spec.INDEX_BITMAP_BLOCK, bitMapBlockDatas);
		// write DescriptorBlocks to disk
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_AMOUNT; i++) {
			byte[] descriptorBlockDatas = this.descriptorBlocks[i].getBytes();
			this.disk.writeBlock(Spec.INDEX_DESCRIPTOR_BLOCK_START + i, descriptorBlockDatas);
		}
		// write DirectoryBlocks to disk
		DescriptorBlock directoryDescriptorBlock = this.descriptorBlocks[0];
		Descriptor directoryFileDescriptor = directoryDescriptorBlock.getDescriptor(0);
		for (int i = 0; i < Spec.DESCRIPTOR_BLOCK_ID_AMOUNT; i++) {
			int directoryDataBlockId = directoryFileDescriptor.getBlockId(i);
			DirectoryDataBlock directoryDataBlock = this.directoryDataBlocks[i];
			byte[] directoryDataBlockDatas = directoryDataBlock.getBytes(); 
			this.disk.writeBlock(directoryDataBlockId, directoryDataBlockDatas);
		}
		// write open file table to disk
		for (int i = 1; i < Spec.OPEN_FILE_TABLE_ENTRY_AMOUNT; i++) {
			if (this.oft.getEntry(i) != null) {
				close(i, true);
			}
		}
		// read disk block as byte[] one by one and save to the file
		FileOutputStream fos = new FileOutputStream(new File(ldiskName));
		for (int i = 0; i < Spec.TOTAL_BLOCK_AMOUNT; i++) {
			byte[] buffer = new byte[Spec.BLOCK_SIZE_IN_BYTE];
			disk.readBlock(i, buffer);
			fos.write(buffer);
		}
		fos.flush();
		fos.close();
		System.out.println("disk saved");
	}

}