package FileSystem;


class OpenFileTable {

	private TableEntry[] tableEntries;

	public OpenFileTable() {
		this.tableEntries = new TableEntry[Spec.OPEN_FILE_TABLE_ENTRY_AMOUNT];
	}
	
	/*
	 * @return index in OpenFileTable if entry is inserted successfully, or -1 if all the 3 file entries are taken 
	 */
	public int insertEntry(byte[] blockDatas, int currentPosition, int descriptorId, int fileLength) {
		for (int i = 0; i < tableEntries.length; i++) {
			if (tableEntries[i] == null) {
				tableEntries[i] = new TableEntry(blockDatas, currentPosition, descriptorId, fileLength);
				return i;
			}
		}
		return -1;
	}
	
	public void removeEntry(int index) {
		assert (0 < index && index < tableEntries.length);
		tableEntries[index] = null;
	}

	public TableEntry getEntry(int index) {
		assert (0 <= index && index < tableEntries.length);
		return this.tableEntries[index];
	}

	public int getEntryIndexWithDescriptorId(int descriptorId) {
		for (int i = 0; i < this.tableEntries.length; i++) {
			TableEntry entry = this.tableEntries[i];
			if (entry != null) {
				if (entry.getDescriptorId() == descriptorId) {
					return i;
				}
			}
		}
		return -1;
	}
}
