package FileSystem;

public class Spec {

	// sizes
	public static final int BLOCK_SIZE_IN_BYTE = 64;
	public static final int DESCRIPTOR_SIZE_IN_BYTE = 16;
	public static final int ENTRY_SIZE_IN_BYTE = 8;

	// block amounts
	public static final int TOTAL_BLOCK_AMOUNT = 64;
	public static final int BITMAP_BLOCK_AMOUNT = 1;
	public static final int DESCRIPTOR_BLOCK_AMOUNT = 6;
	public static final int DATA_BLOCK_AMOUNT = TOTAL_BLOCK_AMOUNT - BITMAP_BLOCK_AMOUNT - DESCRIPTOR_BLOCK_AMOUNT;

	// indexes
	public static final int INDEX_BITMAP_BLOCK = 0;
	public static final int INDEX_DESCRIPTOR_BLOCK_START = 1;
	public static final int INDEX_DESCRIPTOR_BLOCK_END = INDEX_DESCRIPTOR_BLOCK_START + DESCRIPTOR_BLOCK_AMOUNT;

	// other amounts
	public static final int DESCRIPTOR_BLOCK_ID_AMOUNT = 3;
	public static final int DIRECTORY_DATA_BLOCK_ENTRY_AMOUNT = BLOCK_SIZE_IN_BYTE / ENTRY_SIZE_IN_BYTE;
	public static final int DESCRIPTOR_BLOCK_DESCRIPTORS_AMOUNT = BLOCK_SIZE_IN_BYTE / DESCRIPTOR_SIZE_IN_BYTE;
	public static final int OPEN_FILE_TABLE_ENTRY_AMOUNT = 4;

	// bit map
	public static final int BIT_MAP_LENGTH = 2;
	public static final int MASK_SIZE = 32;
	public static final int BIT_MAP_INTS_AMOUNT = BLOCK_SIZE_IN_BYTE / Integer.BYTES;	// 16

	// file constraints
	public static final int FILE_NAME_MAX_LENGTH = 4;
	public static final int MAX_FILE_LENGTH = BLOCK_SIZE_IN_BYTE * DESCRIPTOR_BLOCK_ID_AMOUNT;
	
	// directory block id
	public static final int DIRECTORY_BLOCK_ID = 1;

}
