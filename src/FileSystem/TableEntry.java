package FileSystem;

class TableEntry {
	
	private byte[] rwBuffer;
	private int currentPosition;
	private int descriptorId;
	private int fileLength;
	
	// constructor
	public TableEntry(byte[] blockDatas, int currentPosition, int descriptorId, int fileLength) {
		this.setRwBuffer(blockDatas);
		this.setCurrentPosition(currentPosition);
		this.setDescriptorId(descriptorId);
		this.setFileLength(fileLength);
	}

	// getter and setter
	public byte[] getRwBuffer() {
		return rwBuffer;
	}
	
	public void setRwBuffer(byte[] rwBuffer) {
		this.rwBuffer = rwBuffer;
	}

	public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}

	public int getDescriptorId() {
		return descriptorId;
	}

	public void setDescriptorId(int descriptorId) {
		this.descriptorId = descriptorId;
	}

	public int getFileLength() {
		return fileLength;
	}

	public void setFileLength(int fileLength) {
		this.fileLength = fileLength;
	}

}
