package IOSystem;

import FileSystem.Spec;

public class LDisk {

	// a ldisk will have 1 bitmap block + 6 descriptor blocks + 57 data blocks = 64 blocks in total  
	private byte[][] blocks;

	// constructors
	// init constructor
	public LDisk() {
		blocks = new byte[Spec.TOTAL_BLOCK_AMOUNT][Spec.BLOCK_SIZE_IN_BYTE];
	}
	// restore constructor
	public LDisk(byte[][] blockDatas) {
		assert (blockDatas.length == Spec.TOTAL_BLOCK_AMOUNT);
		blocks = blockDatas;
	}


	// I/O interface
	public void readBlock(int blockId, byte[] readBuffer) {
		byte[] blockData = blocks[blockId]; 
		for (int i = 0; i < blockData.length; i++) {
			byte b = blockData[i];
			readBuffer[i] = b;
		}
	}

	public void writeBlock(int blockId, byte[] writeBuffer) {
		byte[] blockData = blocks[blockId]; 
		for (int i = 0; i < blockData.length; i++) {
			byte b = writeBuffer[i];
			blockData[i] = b;
		}
	}
}